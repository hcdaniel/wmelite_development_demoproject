#!/bin/bash

WINEPROJECTPATH="Z:\\var\\lib\\jenkins\\workspace\\wmelite_development_Android_app\\WMELiteDevelopment\\project\\basic.wpr"

WINEOUTPUTPATH="Z:\\var\\lib\\jenkins\\workspace\\wmelite_development_Android_app\\WMELiteDevelopment\\project\\packages\\"

WINETOOLSPATH="Z:\\var\\lib\\jenkins\\workspace\\wmelite_development_Android_app\\WMELiteDevelopment\\project\\wmetools\\"

# remove old packages
rm -rf /var/lib/jenkins/workspace/wmelite_development_Android_app/WMELiteDevelopment/project/packages/

# projectfile outputdir toolsdir addcrashlib enablelogfilewriting
wine ProjectCompiler.exe $WINEPROJECTPATH $WINEOUTPUTPATH $WINETOOLSPATH true true

